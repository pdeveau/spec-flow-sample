﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Contacts.Abstractions;
using Contacts.Models;

namespace Contacts.Api
{
    public class ContactController : ApiController
    {
        private readonly IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        public async Task<HttpResponseMessage> Get(int id)
        {
            try
            {
                var result = await _contactService.ReadContactByIdAsync(id);

                return result == null
                    ? Request.CreateResponse(HttpStatusCode.NotFound)
                    : Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await _contactService.ReadContactsAsync();

                return result == null
                    ? Request.CreateResponse(HttpStatusCode.NotFound)
                    : Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public async Task<HttpResponseMessage> Put(Contact contact)
        {
            try
            {
                var result = await _contactService.CreateAsync(contact);

                return Request.CreateResponse(HttpStatusCode.Created, result);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public async Task<HttpResponseMessage> DeleteAsync(int id)
        {
            try
            {
                await _contactService.DeleteContactAsync(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}