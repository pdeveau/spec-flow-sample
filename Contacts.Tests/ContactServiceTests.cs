﻿using System;
using System.Threading.Tasks;
using Contacts.Abstractions;
using Contacts.Components;
using Contacts.Models;
using Contacts.TestHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Contacts.Tests
{
    [TestClass]
    public class ContactServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task CreateAsync_WhenContactIsNull_ShouldThrowArgumentNullException()
        {
            IContactService sut = new ContactService(new Mock<IRepository<Contact>>().Object);
            await sut.CreateAsync(default(Contact));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidEntityException))]
        public async Task CreateAsync_WhenContactIsInvalid_ShouldThrowAnException()
        {
            IContactService sut = new ContactService(new Mock<IRepository<Contact>>().Object);
            await sut.CreateAsync(InvalidContact.NullValueContact);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task CreateAsync_WhenContactIsNull_ShouldNotCallRepository()
        {
            var contactRepoMock = new Mock<IRepository<Contact>>();
            contactRepoMock.Setup(setup => setup.AddAsync(It.IsAny<Contact>())).Returns(Task.FromResult(new Contact()));

            IContactService sut = new ContactService(contactRepoMock.Object);
            await sut.CreateAsync(default(Contact));

            contactRepoMock.Verify(verify=>verify.AddAsync(It.IsAny<Contact>()), Times.Never);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidEntityException))]
        public async Task CreateAsync_WhenContactIsInvalid_ShouldNotCallRepostitory()
        {
            var contactRepoMock = new Mock<IRepository<Contact>>();
            contactRepoMock.Setup(setup => setup.AddAsync(It.IsAny<Contact>())).Returns(Task.FromResult(new Contact()));

            IContactService sut = new ContactService(contactRepoMock.Object);
            await sut.CreateAsync(InvalidContact.NullValueContact);

            contactRepoMock.Verify(verify => verify.AddAsync(It.IsAny<Contact>()), Times.Never);
        }

        [TestMethod]
        public async Task CreateAsync_WhenEntityIsValid_ShouldCallRepositoryOneTime()
        {
            var contactRepoMock = new Mock<IRepository<Contact>>();
            contactRepoMock.Setup(setup => setup.AddAsync(It.IsAny<Contact>())).Returns(Task.FromResult(new Contact()));
            IContactService sut = new ContactService(contactRepoMock.Object);
            await sut.CreateAsync(ValidContact.Instance);
        }
    }
}
