﻿using System;
using System.Net;
using System.Threading.Tasks;
using Contacts.Abstractions;
using Contacts.Api;
using Contacts.Models;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Contacts.Tests
{
    [TestClass]
    public class ContactControllerTests
    {
        [TestMethod]
        public async Task Get_WhenCalledWithAnIdThatExists_ShouldReturnA200()
        {
            var contactServiceMock = new Mock<IContactService>();

            contactServiceMock.Setup(setup => setup.ReadContactByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Contact()));

            var controller = ControllerContextHelper<ContactController>.GetControllerContext(contactServiceMock.Object);

            const int anyId = 1;

            var result = await controller.Get(anyId);
            result.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task Get_WhenCalledWithAnIdThatDoesNotExist_ShouldReturnA404()
        {
            var contactServiceMock = new Mock<IContactService>();

            contactServiceMock.Setup(setup => setup.ReadContactByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(default(Contact)));

            var controller = ControllerContextHelper<ContactController>.GetControllerContext(contactServiceMock.Object);

            const int invalidId = 0;
            var result = await controller.Get(invalidId);

            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task Get_WhenCalledWithAnId_AndAnythingGoesWrong_ShouldReturnA500()
        {
            var contactServiceMock = new Mock<IContactService>();

            contactServiceMock.Setup(setup => setup.ReadContactByIdAsync(It.IsAny<int>()))
                .Throws<Exception>();

            var controller = ControllerContextHelper<ContactController>.GetControllerContext(contactServiceMock.Object);

            const int anyValidId = 1;
            var result = await controller.Get(anyValidId);

            result.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        [TestMethod]
        public async Task Get_WhenCalledWithoutIssue_ShouldDelegateToContactReader_OneTime()
        {
            var contactServiceMock = new Mock<IContactService>();

            contactServiceMock.Setup(setup => setup.ReadContactByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new Contact()));

            var controller = ControllerContextHelper<ContactController>.GetControllerContext(contactServiceMock.Object);

            const int anyValidId = 1;

            await controller.Get(anyValidId);

            contactServiceMock.Verify(verify => verify.ReadContactByIdAsync(It.IsAny<int>()));
        }

        [TestMethod]
        public async Task Put_WhenCalledSuccessfully_ShouldReturnA201()
        {
            var controller =
                ControllerContextHelper<ContactController>.GetControllerContext(new Mock<IContactService>().Object);

            var result = await controller.Put(new Contact());

            result.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [TestMethod]
        public async Task Put_WhenSomethingGoesWrong_ShouldReturnA500()
        {
            var contactServiceMock = new Mock<IContactService>();
            contactServiceMock.Setup(setup => setup.CreateAsync(It.IsAny<Contact>()))
                .Throws<Exception>();

            var controller =
                ControllerContextHelper<ContactController>.GetControllerContext(contactServiceMock.Object);

            var result = await controller.Put(new Contact());

            result.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        [TestMethod]
        public async Task Put_WhenCalled_ShouldDelegateToContactCreator_OneTime()
        {
            var contactServiceMock = new Mock<IContactService>();
            contactServiceMock.Setup(setup => setup.CreateAsync(It.IsAny<Contact>()))
                .Returns(Task.FromResult(new Contact()));

            var controller =
                ControllerContextHelper<ContactController>.GetControllerContext(contactServiceMock.Object);

            await controller.Put(new Contact());

            contactServiceMock.Verify(verify => verify.CreateAsync(It.IsAny<Contact>()), Times.Once);
        }

        [TestMethod]
        public async Task Delete_WhenCalledSuccessfully_ShouldReturnA200()
        {
            var controller =
                ControllerContextHelper<ContactController>.GetControllerContext(new Mock<IContactService>().Object);

            const int validIdForDelete = 1;

            var result = await controller.DeleteAsync(validIdForDelete);

            result.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
