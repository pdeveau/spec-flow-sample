using Contacts.Models;

namespace Contacts.Tests
{
    internal class ValidContact
    {
        public static Contact Instance { get; }

        static ValidContact()
        {
            Instance = new Contact
            {
                Firstname = nameof(Contact.Firstname),
                Lastname = nameof(Contact.Lastname)
            };
        }

        protected ValidContact()
        {
            
        }
    }
}