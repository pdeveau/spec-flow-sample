﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Contacts.Tests
{
    public static class ControllerContextHelper<T> where T : ApiController
    {
        public static T GetControllerContext(params object[] arguments)
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage();

            request.SetConfiguration(config);

            var controllerContext = new HttpControllerContext
            {
                Configuration = config,
                Request = request
            };

            var controller = typeof(T).GetConstructors().First().Invoke(arguments) as ApiController;
            if (controller == null) throw new Exception("Could not construct controller");

            controller.ControllerContext = controllerContext;

            return controller as T;
        }
    }
}
