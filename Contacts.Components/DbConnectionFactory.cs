﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Contacts.Abstractions;

namespace Contacts.Components
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private static readonly string ConnectionString;

        static DbConnectionFactory()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["Contacts"].ConnectionString;
        }

        public IDbConnection GetConnection()
        {
            var sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();
            return sqlConnection;
        }
    }
}