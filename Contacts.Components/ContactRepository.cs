﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contacts.Abstractions;
using Contacts.Models;
using Dapper;

namespace Contacts.Components
{
    public class ContactRepository : IRepository<Contact>
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public ContactRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<Contact> AddAsync(Contact entity)
        {
            using (var connection = _dbConnectionFactory.GetConnection())
            {
                entity.Id = (await connection.QueryAsync<int>(
                    "INSERT INTO dbo.Contact(FirstName, LastName) VALUES (@Firstname, @Lastname); SELECT CAST(SCOPE_IDENTITY() as INT) as Id",
                    entity)).First();

                return entity;
            }
        }

        public async Task<Contact> ReadByIdAsync(int id)
        {
            using (var connection = _dbConnectionFactory.GetConnection())
            {
                return (await connection.QueryAsync<Contact>(
                    "SELECT c.Id as Id, c.Firstname as Firstname, c.Lastname as Lastname FROM dbo.Contact c WHERE c.Id = @Id",
                    new {Id = id})).First();
            }
        }

        public async Task<IEnumerable<Contact>> ReadAsync()
        {
            using (var connection = _dbConnectionFactory.GetConnection())
            {
                return await connection.QueryAsync<Contact>(
                    "SELECT c.Id as Id, c.Firstname as Firstname, c.Lastname as Lastname FROM dbo.Contact c");
            }
        }

        public async Task<bool> UpdateAsync(Contact entity)
        {
            using (var connection = _dbConnectionFactory.GetConnection())
            {
                var updated = await connection.ExecuteAsync(
                    "UPDATE dbo.Contact SET Firstname = @Firstname, Lastname = @Lastname WHERE Id = @Id", entity);

                return updated > 0;
            }
        }

        public async Task<bool> DeleteAsync(Contact entity)
        {
            using (var connection = _dbConnectionFactory.GetConnection())
            {
                var deleted = await connection.ExecuteAsync("DELETE FROM dbo.Contact WHERE Id = @Id", entity);

                return deleted > 0;
            }
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            using (var connection = _dbConnectionFactory.GetConnection())
            {
                var deleted = await connection.ExecuteAsync("DELETE FROM dbo.Contact WHERE Id = @Id", new {Id = id});

                return deleted > 0;
            }
        }
    }
}
