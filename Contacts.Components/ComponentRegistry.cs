﻿using Contacts.Abstractions;
using Contacts.Models;
using StructureMap;

namespace Contacts.Components
{
    public class ComponentRegistry : Registry
    {
        public ComponentRegistry()
        {
            For<IContactService>().Use<ContactService>();
            For<IRepository<Contact>>().Use<ContactRepository>();
            For<IDbConnectionFactory>().Use<DbConnectionFactory>();
        }
    }
}
