﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contacts.Abstractions;
using Contacts.Models;
using FluentValidation;

namespace Contacts.Components
{
    public class ContactService : IContactService
    {
        private readonly IRepository<Contact> _contactRepository;
        private readonly ContactValidator _validator;

        public ContactService(IRepository<Contact> contactRepository)
        {
            _contactRepository = contactRepository;
            _validator = new ContactValidator();
        }

        public async Task<Contact> CreateAsync(Contact contact)
        {
            if (contact == null) throw new ArgumentNullException(nameof(contact));

            var validationResult = _validator.Validate(contact);

            if(!validationResult.IsValid) throw new InvalidEntityException();

            return await _contactRepository.AddAsync(contact);
        }

        public async Task<Contact> ReadContactByIdAsync(int id)
        {
            if(id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
            return await _contactRepository.ReadByIdAsync(id);
        }

        public async Task<IEnumerable<Contact>> ReadContactsAsync()
        {
            return await _contactRepository.ReadAsync();
        }

        public async Task<bool> UpdateContactAsync(Contact contact)
        {
            if (contact == null) throw new ArgumentNullException(nameof(contact));

            var validationResult = _validator.Validate(contact);

            if (!validationResult.IsValid) throw new InvalidEntityException();

            return await _contactRepository.UpdateAsync(contact);
        }

        Task<bool> IContactService.DeleteContactAsync(int id)
        {
            if (id <= 0) throw new ArgumentOutOfRangeException();

            return _contactRepository.DeleteByIdAsync(id);
        }
    }

    internal class ContactValidator : AbstractValidator<Contact>
    {
        public ContactValidator()
        {
            RuleFor(contact => contact).NotNull();
            RuleFor(contact => contact.Lastname).NotNull().NotEmpty().Length(3, 250);
            RuleFor(contact => contact.Firstname).NotNull().NotEmpty().Length(3, 250);
        }
    }
}