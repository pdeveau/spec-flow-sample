﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Contacts.BehaviorTests
{
    public abstract class TestResolver<T> where T : class
    {
        public virtual T Resolve()
        {
            var type = typeof(T);
            var constructor = type.GetConstructors().First();
            var parameters = constructor.GetParameters();

            var container = Bootstrapper.Initialize();
            var types = new object[parameters.Length];

            for (var idx = 0; idx < parameters.Length; idx++)
            {
                var parameter = parameters[idx];
                types[idx] = container.GetInstance(parameter.ParameterType);
            }

            return constructor.Invoke(types) as T;
        }
    }

    public abstract class TestControllerResolver<T> : TestResolver<T> where T : ApiController
    {
        public override T Resolve()
        {
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage();

            request.SetConfiguration(config);

            var controllerContext = new HttpControllerContext
            {
                Configuration = config,
                Request = request
            };

            var controller = base.Resolve();
            
            controller.ControllerContext = controllerContext;

            return controller;
        }
    }
}
