﻿Feature: Contact
	I want to be able to add and remove contacts
	So that I can keep current with people that I normally talk to

Scenario: Create a Contact
	Given I have a new contact
	When The contact already exists
	And I add the new contact
	Then The contact should not be added to the database