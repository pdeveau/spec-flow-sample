﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Contacts.Api;
using Contacts.Models;
using FluentAssertions;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace Contacts.BehaviorTests.Steps
{
    [Binding]
    public class ContactSteps : TestControllerResolver<ContactController>
    {
        private Contact _contact;
        private ContactController _sut;

        [Given(@"I have a new contact")]
        public void GivenIHaveANewContact()
        {
            _sut = Resolve();

            _contact = new Contact
            {
                Firstname = "Phil",
                Lastname = "DeVeau"
            };
        }
        
        [When(@"The contact already exists")]
        public async void WhenTheContactAlreadyExists()
        {
            var duplicateContact = new Contact
            {
                Firstname = "Phil",
                Lastname = "DeVeau"
            };

            await _sut.Put(duplicateContact);
        }

        [When(@"I add the new contact")]
        public async void WhenIAddTheNewContact()
        {
            await _sut.Put(_contact);
        }

        [Then(@"The contact should not be added to the database")]
        public void ThenTheContactShouldNotBeAddedToTheDatabase()
        {
            var result = _sut.Get().Result;

            var stream = result.Content.ReadAsStreamAsync().Result;
            var reader = new StreamReader(stream);

            var serializer = new JsonSerializer();
            var end = serializer.Deserialize<List<Contact>>(new JsonTextReader(reader));
            
            end.Count(c => c.Lastname == _contact.Lastname && c.Firstname == _contact.Firstname).Should().Be(1);
        }

        [Given(@"That ""(.*)"" Likes Chicken Nuggets")]
        public void GivenThatLikesChickenNuggets(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"The chicken nuggets are raw")]
        public void WhenTheChickenNuggetsAreRaw()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"""(.*)"" won't eat them")]
        public void ThenWonTEatThem(string p0)
        {
            ScenarioContext.Current.Pending();
        }

    }
}


