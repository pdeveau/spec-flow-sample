﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contacts.Api;
using Contacts.Components;
using StructureMap;

namespace Contacts.BehaviorTests
{
    internal static class Bootstrapper
    {
        public static IContainer Initialize()
        {
            return new Container(container =>
            {
                container.AddRegistry<ComponentRegistry>();
                container.AddRegistry<BootstrapRegistry>();
            });
        }
    }
}
