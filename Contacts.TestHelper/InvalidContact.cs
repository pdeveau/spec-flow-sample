using Contacts.Models;

namespace Contacts.TestHelper
{
    public class InvalidContact
    {
        public static Contact NullValueContact { get; }
        public static Contact OutOfRangeContact { get; }

        static InvalidContact()
        {
            NullValueContact = new Contact();
            OutOfRangeContact = new Contact
            {
                Firstname = string.Empty.PadLeft(5000, '0'),
                Lastname = string.Empty.PadLeft(5000, '0')
            };
        }

        protected InvalidContact()
        {
            
        }
    }
}