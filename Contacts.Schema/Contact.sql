﻿CREATE TABLE [dbo].[Contact]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Firstname] nvarchar(100) not null,
	[Lastname] nvarchar(100) not null
)
