﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contacts.Abstractions
{
    public interface IRepository<T>
    {
        Task<T> AddAsync(T entity);
        Task<T> ReadByIdAsync(int id);
        Task<IEnumerable<T>> ReadAsync();
        Task<bool> UpdateAsync(T entity);
        Task<bool> DeleteAsync(T entity);
        Task<bool> DeleteByIdAsync(int id);
    }
}