﻿using System.Data;

namespace Contacts.Abstractions
{
    public interface IDbConnectionFactory
    {
        IDbConnection GetConnection();
    }
}