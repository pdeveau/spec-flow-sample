﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contacts.Models;

namespace Contacts.Abstractions
{
    public interface IContactService
    {
        Task<Contact> CreateAsync(Contact contact);
        Task<Contact> ReadContactByIdAsync(int id);
        Task<IEnumerable<Contact>> ReadContactsAsync();
        Task<bool> UpdateContactAsync(Contact contact);
        Task<bool> DeleteContactAsync(int id);
    }
}