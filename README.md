# A simple project that demonstrates a basic feature file and using SpecFlow as an integration test. #

# Links to Documentation #

* [Gherkin Docs](https://github.com/cucumber/cucumber/wiki/Gherkin)
* [Cucumber Docs](https://cucumber.io/docs)
* [Pickles Docs](http://www.picklesdoc.com/)
* [SpecFlow Docs](http://specflow.org/documentation/)
* [Google Groups for Specflow](https://groups.google.com/forum/#!forum/specflow)
* [Pluralsight SpecFlow Course](https://www.pluralsight.com/courses/specflow-2-0-business-readable-automated-tests)

# Getting Specflow to work with MSTest #

```
#!XML

<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
    <section name="specFlow" type="TechTalk.SpecFlow.Configuration.ConfigurationSectionHandler, TechTalk.SpecFlow"/>
  </configSections>

  <specFlow>
    <unitTestProvider name="MsTest" />
  </specFlow>
</configuration>
```